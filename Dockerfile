FROM node:12.16.2

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes
ENV PANDOC_VERSION 2.14.1
ENV NODE_ENV production

RUN apt-get autoremove && apt-get autoclean

RUN apt-get -qq -y --allow-unauthenticated update && \
    apt-get -qq -y --allow-unauthenticated upgrade && \
    apt-get -qq -y --allow-unauthenticated install \
    wget \
    texlive-latex-base \
    texlive-fonts-recommended \
    texlive-fonts-extra \
    texlive-latex-extra \
    texlive-full \
    build-essential \
    checkinstall \
    libreadline-gplv2-dev \
    libncursesw5-dev \
    libssl-dev \
    libsqlite3-dev \
    tk-dev \
    libgdbm-dev \
    libc6-dev \
    libbz2-dev \
    libffi-dev \
    zlib1g-dev \
    python3-pip \
    cron \
    poppler-utils \
    lua5.3

RUN apt-get clean

RUN wget https://api.dev.brezel.io/pandoc-2.14.1-1-amd64.deb && \
    dpkg -i pandoc* && \
    rm pandoc*

RUN mkdir /app && \
    mkdir /app/assets && \
    mkdir /app/output

WORKDIR /app

COPY ./ /app

RUN python3 get-pip.py
RUN python3 -m pip install docxcompose
RUN python3 -m pip install PyPDF2
RUN python3 -m pip install pdf2image

RUN cd pandocfilters && python3 setup.py install

RUN find /app/filters -type f -iname "*.py" -exec chmod +x {} \;
RUN find /app/filters -type f -iname "*.lua" -exec chmod +x {} \;

# Copy hello-cron file to the cron.d directory
COPY delete_old_assets /etc/cron.d/delete_old_assets

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/delete_old_assets

# Apply cron job
RUN crontab /etc/cron.d/delete_old_assets

RUN npm install

EXPOSE 80

CMD cron && node --max-http-header-size 1048576000 server.js
