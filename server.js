const http = require('http');
const https = require('https');
const fs = require('fs-promise');
const path = require('path');
const mediaTypeConverter = require('./mediatype-converter');
const rawBody = require('raw-body');
const lignator = require('lignator');
const Stream = require('stream').Transform;
const spawn = require('child_process').spawn;

const PizZip = require('pizzip');
const Docxtemplater = require('docxtemplater');

const pandocPath = process.env.PANDOC || 'pandoc';
const pdflatexPath = process.env.PDFLATEX || 'pdflatex';
const docxComposePath = process.env.DOCXCOMPOSE || 'docxcompose';
const python3 = process.env.PYTHON3 || 'python3';

const port = 80;

/**
 * Uses the pandoc command-line tool to convert the input file into the desired output format.
 *
 * @param {String} inputFile Path to the input File
 * @param {String} outputFile Path to the output file
 * @param {String} from Type of the input file
 * @param {String} to Desired type of the output file
 * @param {String} filters in valid JSON
 * @param uniqueIdentifier
 * @param bibs
 * @param language
 * @param variables
 * @return {Promise} Resolves if conversion is successful, otherwise it reject with a corresponding error message.
 */
function pandoc(inputFile, outputFile, from, to, filters, uniqueIdentifier, bibs, language, variables) {
    let args = ['--extract-media=./assets'];
    if(variables.toc) {
        args.push('--toc');
    }
    if (filters) {
        filters = JSON.parse(filters);
        for (let filter of filters) {
            args.push('--filter');
            args.push('./filters/' + filter + '.py');
        }
    }
    if (bibs !== null) {
        args.push('--bibliography=' + uniqueIdentifier + '.bib');
    }

    return new Promise((resolve, reject) => {
        if (from === 'latex' && to === 'docx') {
            let pythonError = '';
            console.log('Starting fixLatexPdfInclude with these Args: ');
            console.log(inputFile);
            let python = spawn(python3, ['fixLatexPdfInclude.py', inputFile]);
            python.on('error', (err) => reject(err));

            python.stderr.on('data', (data) => pythonError += data);

            python.on('close', (code) => {
                if (code !== 0) {
                    let msg = 'PdfIncludeFix finished with exit code ' + code;
                    if (pythonError) {
                        msg += ':' + pythonError;
                    }
                    return reject(msg);
                }
                let tocTitle = 'Inhaltsverzeichnis';
                if (language === 'en') {
                    tocTitle = 'Table of Contents';
                }
                args = args.concat(['--lua-filter=filters/pagebreak.lua', '--metadata', 'toc-title:' + tocTitle, '--metadata', 'link-citations:true', '--citeproc', '--csl', 'ieee.csl', '-f', from + '+raw_tex', '-t', to, '-o', 'output/' + outputFile, inputFile]);
                console.log('Starting Pandoc with these Args: ');
                console.log(args);
                let pandoc = spawn(pandocPath, args);

                let error = '';
                pandoc.on('error', (err) => reject(err));

                pandoc.stderr.on('data', (data) => error += data);

                pandoc.on('close', (code) => {
                    if (code !== 0) {
                        let msg = 'Pandoc finished with exit code ' + code;
                        if (error) {
                            msg += ':' + error;
                        }
                        reject(msg);
                    } else {
                        resolve();
                    }
                });

            });
        } else {
            args = args.concat(['-f', from, '-t', to, '-o', 'output/' + outputFile, inputFile]);
            console.log('Starting Pandoc with these Args: ');
            console.log(args);
            let pandoc = spawn(pandocPath, args);

            let error = '';
            pandoc.on('error', (err) => reject(err));

            pandoc.stderr.on('data', (data) => error += data);

            pandoc.on('close', (code) => {
                if (code !== 0) {
                    let msg = 'Pandoc finished with exit code ' + code;
                    if (error) {
                        msg += ':' + error;
                    }
                    reject(msg);
                } else {
                    resolve();
                }
            });
        }
    });
}

/**
 * Start PdfLatex directly and generate a Pdf
 *
 * @param inputFile Inputfile name
 * @param outputFile Outputfile name
 * @returns {Promise}
 */
function pdflatex(inputFile, outputFile) {
    return new Promise((resolve, reject) => {
        let args = ['-synctex=1', '-interaction=nonstopmode', '-output-directory=output', '-jobname', outputFile.slice(0, -4), inputFile];
        console.log('Starting PDFLateX with these Args: ');
        console.log(args);
        let pdflatex = spawn(pdflatexPath, args);

        pdflatex.on('error', (err) => reject(err));

        pdflatex.stdout.on('data', function(data) {
            if (data.toString().includes('Error')) {
                console.log(data.toString());
            }
        });

        pdflatex.on('close', (code) => {
            let msg = 'PDFLateX close with code: ' + code;
            fs.access(__dirname + '/output/' + outputFile, fs.F_OK, (e) => {
                if (e) {
                    console.log(msg + ' : ' + e);
                    reject('PDFLateX failed to create a file. Please check your input for invalid LateX!');
                } else {
                    resolve();
                }
            });
        });
    });
}

/**
 * Manages methods of converting the files.
 * Default is using Pandoc, LateX to PDF will use PDFLateX directly.
 * If we use PDFLateX directly we have to call it twice to enable a ToC.
 *
 * @param inputFile Inputfile Name
 * @param outputFile Outputfile Name
 * @param inputType
 * @param pandocOutputType
 * @param filters
 * @param latexReferenceFolder
 * @returns {Promise}
 */
function convert(inputFile, outputFile, inputType, pandocOutputType, filters, uniqueIdentifier, bibs, language, variables) {
    if (inputType === 'latex' && pandocOutputType === 'pdf') {
        console.log('Using PdfLateX to convert Latex to PDF directly.');
        return new Promise((resolve, reject) => {
            pdflatex(inputFile, outputFile)
                .then(() => pdflatex(inputFile, outputFile))
                .then(() => resolve())
                .catch((err) => reject(err));
        });
    } else {
        return pandoc(inputFile, outputFile, inputType, pandocOutputType, filters, uniqueIdentifier, bibs, language, variables);
    }
}

function prependIfDocx(outputFileName, pandocOutputType, variables, language) {
    if (pandocOutputType !== 'docx') {
        return Promise.resolve();
    }
    if (variables == null || variables.titlepageFilename == null) {
        return Promise.resolve();
    }

    console.log('output type docx and titelpageFilename var is set, adding Lemikos Cover...');

    let folder = 'ref' + Date.now();
    let stylesFilename = variables.stylesFilename ? variables.stylesFilename : 'styles.docx';

    return new Promise((resolve, reject) => {
        fs.copy('references/' + stylesFilename, folder + '/styles.docx').then(() => {
            replaceVariablesInFile(folder + '/styles.docx', variables);
            let titlepageFileName = variables.titlepageFilename;
            fs.copy('references/' + titlepageFileName, folder + '/titlepage.docx').then(() => {
                replaceVariablesInFile(folder + '/titlepage.docx', variables);
                let args = [folder + '/styles.docx', folder + '/titlepage.docx', __dirname + '/output/' + outputFileName, '-o', __dirname + '/output/' + outputFileName];
                console.log('Starting DocxCompose with these Args: ');
                console.log(args);
                let docxCompose = spawn(docxComposePath, args);

                let error = '';
                docxCompose.on('error', (err) => reject(err));

                docxCompose.stderr.on('data', (data) => error += data);

                docxCompose.on('close', (code) => {
                    if (code !== 0) {
                        let msg = 'DocxCompose finished with exit code ' + code;
                        if (error) {
                            msg += ':' + error;
                        }
                        reject(msg);
                    } else {
                        resolve();
                    }
                });

            });

        });
    });
}

/**
 * Downloads a single Asset
 *
 * @param url Url to an Image
 * @param dest Path where it will be saved
 * @returns {Promise}
 */
function download(url, dest) {
    console.log('Downloading Asset from: ' + url + ' to ' + dest);

    let link = new URL(url);
    let client = (link.protocol.includes('https')) ? https : http;

    return new Promise((resolve, reject) => {
        client.request(url, function(response) {
            let data = new Stream();

            response.on('error', function() {
                reject();
            });

            response.on('data', function(chunk) {
                data.push(chunk);
            });

            response.on('end', function() {
                fs.writeFileSync(dest, data.read());
                resolve();
            });
        }).end();
    });
}

function createFromBase64(data, dest) {
    console.log('Decoding Asset and saving to ' + dest);

    return new Promise((resolve, reject) => {
        let rawData = data.split(';base64,').pop();

        try {
            fs.writeFileSync(dest, rawData, {encoding: 'base64'});
            resolve();
        } catch (e) {
            reject('Converting Asset from Base64 to File failed: ' + e);
        }
    });
}

/**
 * Downloads all needed Assets to /assets/
 *
 * @param assets Array of Objects that contain urls and names
 * @returns {Promise}
 */
function downloadAssets(assets) {
    return new Promise((resolve, reject) => {
        let requests = [];

        if (assets) {
            assets = JSON.parse(assets);
            for (let asset of assets) {
                if (asset.url) {
                    requests.push(download(asset.url, './assets/' + asset.name));
                } else {
                    requests.push(createFromBase64(asset.data, './assets/' + asset.name));
                }
            }
        }

        Promise.all(requests)
            .catch((err) => {
                console.log(err);
                reject('Download of one or more assets failed!');
            })
            .then(() => {
                resolve();
            });
    });
}

/**
 * Removes all files for the current request
 *
 * @param outputFile
 * @param inputFile
 * @returns Promise
 */
function removeRequestFiles(outputFile, inputFile) {
    return;
    return new Promise((resolve, reject) => {
        let operations = [];

        operations.push(fs.unlink(inputFile));

        const path = __dirname + '/output/';
        let regex = new RegExp(outputFile.slice(0, -4) + '\..*');
        fs.readdirSync(path)
            .filter((f) => regex.test(f))
            .map((f) => operations.push(fs.unlink(path + f)));

        Promise.all(operations)
            .catch((err) => {
                console.log(err);
                reject('Something went wrong, could not remove all files.');
            })
            .then(() => {
                // lignator.remove('assets', false);

                resolve();
            });
    });
}


function replaceVariablesInFile(filePath, variables) {
    // Load the docx file as binary content
    console.log('Rendering: ' + filePath);
    const content = fs.readFileSync(path.resolve(filePath), 'binary');
    const zip = new PizZip(content);
    const options = {
        nullGetter: () => { return '' },
        paragraphLoop: true,
        linebreaks: true
    }
    const doc = new Docxtemplater(zip, options);
    doc.setData(variables);
    doc.render();
    const buf = doc.getZip().generate({type: 'nodebuffer'});
    fs.writeFileSync(path.resolve(filePath), buf);
}


function buildBib(bibs, uniqueIdentifier) {
    if (bibs == null) {
        return;
    }
    return new Promise((resolve, reject) => {
        let result = '';
        for (let bib of bibs) {
            result += '@webpage{' + bib.bibliography_identifier + ',\n' +
                'date = "' + escapeDoubleQuotes(bib.bibliography_date) + '",\n' +
                'title = "{' + escapeDoubleQuotes(bib.bibliography_name) + '}",\n' +
                'howpublished = "\\url{' + escapeDoubleQuotes(bib.bibliography_url) + '}"\n' +
                '}\n';
        }
        fs.writeFile(uniqueIdentifier + '.bib', result, (err) => {
            if (err) {
                reject();
            }
            resolve();
        });
    });
}

function escapeDoubleQuotes(str) {
    if (str === null) {
        return '';
    }
    return str.replace(/\\([\s\S])|(")/g, '\\$1$2');
}


async function createFile(fileContents) {
    const uniqueFilename = 'render_' + Date.now() + '.docx';
    await fs.writeFile(uniqueFilename, fileContents, 'base64');
    return uniqueFilename;
}

async function renderDocx(requestBody, variables) {
    const fileContents = requestBody['data'];
    const docxFilepath = await createFile(fileContents);
    await replaceVariablesInFile(docxFilepath, variables);
    return fs.readFile(__dirname + '/' + docxFilepath);
}

/**
 * Handles incoming HTTP request. Only POST requests are accepted and the header fields accept and content-type must be
 * set. Otherwise the error code 400 is returned.
 *
 * For proper request, the request body is converted into the accepted output format.
 *
 * @param {http.IncomingMessage} req HTTP request
 * @param {Object} res HTTP response
 */
async function handleRequest(req, res) {

    console.log('Request received at [' + (new Date).toLocaleString() + ']');
    if (req.method !== 'POST' || !req.headers['content-type'] || !req.headers['accept']) {
        res.statusCode = 400;
        res.statusMessage = 'Bad Request';
        res.end('Only POST is supported');
        return;
    }

    const buffers = [];

    for await (const chunk of req) {
        buffers.push(chunk);
    }

    req.on('data', (data) => {
       console.log(data)
    });

    const data = Buffer.concat(buffers).toString();


    let reqBody = JSON.parse(data);

    let variables = reqBody['variables'];
    if (variables) {
        variables = JSON.parse(variables);
    }

     // TODO: docx rendering doesn't even use Pandoc! Why is it in this Project?!
    if(req.url === '/renderDocx') {
        let result = await renderDocx(reqBody, variables);
        res.setHeader('Content-Type', "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        return res.end(result);
    }

    let assets = reqBody['assets'];
    let filters = reqBody['filters'];
    let language = reqBody['language'];
    console.log('Language: ' + language);
    let bibs = reqBody['bibliography'];
    if (bibs) {
        bibs = JSON.parse(bibs);
    }
    let inputType = mediaTypeConverter(req.headers['content-type']);
    let outputMediaType = req.headers['accept'];
    let pandocOutputType = mediaTypeConverter(req.headers['accept']);
    console.log('Pandoc input type: ', inputType);
    console.log('Pandoc output type: ', pandocOutputType);

    let uniqueIdentifier = Date.now();
    let inputFile = 'in' + uniqueIdentifier;
    let outputFile = 'out' + uniqueIdentifier;

    // Pandoc only supports pdf via latex
    if (pandocOutputType === 'pdf') {
        outputFile += '.pdf';
        if (inputType === 'latex') {
            inputFile += '.tex';
        } else {
            pandocOutputType = 'latex';
        }
    }

    fs.writeFile(inputFile, reqBody['data'])
        .then(() => downloadAssets(assets))
        .then(() => buildBib(bibs, uniqueIdentifier))
        .then(() => convert(inputFile, outputFile, inputType, pandocOutputType, filters, uniqueIdentifier, bibs, language, variables))
        .then(() => prependIfDocx(outputFile, pandocOutputType, variables, language))
        .then(() => fs.readFile(__dirname + '/output/' + outputFile))
        .then((result) => {
            res.setHeader('Content-Type', outputMediaType);
            res.setHeader('Process-Id', uniqueIdentifier);
            res.end(result);
            console.log('Success!');
        })
        .catch((err) => {
            console.error('Error during conversion: ', err);
            res.statusCode = 500;
            res.statusMessage = JSON.stringify('Internal Server Error. ' + err);
            res.end(res.statusMessage);
        })
        .then(() => console.log('Cleaning Up...'))
        .then(() => removeRequestFiles(outputFile, inputFile))
        .then(() => console.log('Ready to handle a new Request.'));
}

let server = http.createServer(handleRequest);

let p = process.env.PORT || port;
server.listen(p, () => console.log('Server listening on port ' + p));
