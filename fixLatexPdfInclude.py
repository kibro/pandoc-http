import re as regex
import sys

from PyPDF2 import PdfFileWriter, PdfFileReader
from pdf2image import pdfinfo_from_path, convert_from_path


def check_argv():
    if len(sys.argv) != 2:
        sys.exit()


def get_filename():
    check_argv()
    return sys.argv[1]


def get_file_contents(filename):
    with open(filename, 'r', encoding="latin-1") as file:
        filedata = file.read()

    return filedata


def write_file_contents(filename, filedata):
    with open(filename, 'w', encoding="latin-1") as file:
        file.write(filedata)


def open_pdf(pdf_name):
    return PdfFileReader(open("assets/" + pdf_name, "rb"), strict=False)


def split_into_images(pdf_name):
    print("splitting: " + pdf_name)

    info = pdfinfo_from_path(pdf_name, userpw=None, poppler_path=None)
    maxPages = info["Pages"]
    pages = []
    for page in range(1, maxPages + 1, 10):
        pages += convert_from_path(pdf_name, dpi=200, first_page=page, last_page=min(page + 10 - 1, maxPages))

    count = 0
    for page in pages:
        print("page: " + str(count))
        page.save(str(pdf_name[:-4]) + "-" + str(count) + ".jpeg", 'JPEG')
        count += 1


def extract_site_from_pdf_into_new_pdf(page, input_pdf):
    output = PdfFileWriter()
    output.addPage(input_pdf.getPage(page))
    return output


def get_page_count_for_pdf_by_name(pdf_name):
    pdf_file = open_pdf(pdf_name)
    return pdf_file.numPages


def replace(match):
    pdf_name = match.group(1)
    split_into_images("assets/" + pdf_name)
    page_count = get_page_count_for_pdf_by_name(pdf_name)

    result = ""
    for page in range(page_count):
        result += "\n\\includegraphics[]{./assets/" + str(pdf_name[:-4]) + "-" + str(page) + ".jpeg}"

    return result


if __name__ == "__main__":
    latex_string = get_file_contents(get_filename())

    latex_string = regex.sub(r"\\includepdf\[pages=-\]\{(.*?)\}", replace, latex_string)

    write_file_contents(get_filename(), latex_string)
